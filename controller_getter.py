import serial
import rospy
from std_msgs.msg import String

ser = serial.Serial('/dev/ttyACM0', 9600)
print "ser start"
rospy.init_node('controller_getter')
print "node init"

pub = rospy.Publisher('joystick', String)
print "pub created"

while not rospy.is_shutdown():
    line = ser.readline()
    print line
    pub.publish(line)
    rospy.sleep(0.05)
