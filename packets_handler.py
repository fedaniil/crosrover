import rospy
from math import pi
from std_msgs.msg import String
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan


def getData(data):
    global queue, cur, need_break
    data = data.data
    if (cur < len(queue)):
        need_break = True
    queue = [i.trim() for i in data.split("|")]
    cur = 0


def updateOdom(data):
    global cur_pos
    cur_pos = data


def updateScan(new_scan):
    global scan
    scan = new_scan


def motorsSend(lin, ang):
    global move
    move.publish(str(lin) + "|" + str(ang) + "|0.06")


def motoreStop():
    motorsSend(0, 0)


def linDist(fir, sec):
    return abs(fir.pose.pose.position.x - sec.pose.pose.position.x)


def angDist(fir, sec):
    return abs(fir.pose.pose.orientation.z - sec.pose.pose.orintation.z)


def canMove():
    global scan
    min_dist = min(min([i if scan.range_min <= i <= scan.range_max for i in scan.ranges[:30]]), min([i if scan.range_min <= i <= scan.range_max for i in scan.ranges[330:]]))
    if (min_dist < 0.3): # dist to diagonal
        return false
    return true


queue = []
cur = 0
cur_pos = Odometry()
scan = LaserScan()
rospy.init_node("packets_cmd")
rospy.Subsbriber("cmd", String, getData)
rospy.Subscriber("odom", Odometry, updateOdom)
rospy.Subscriber("scan", LaserScan, updateScan)
move = rospy.Publisher("move", String)

while not rospy.is_shutdown():
    if (cur < len(queue)):
        cur_cmd = queue[cur]
        if cur_cmd == "stop":
            motorsStop()
        elif cur_cmd.startswith("move"):
            start_pos = cur_pos
            meters = float(cur_cmd.split(" ")[1])
            while not need_break and linDist(start_pos, cur_pos) < abs(meters):
                if meters > 0:
                    motorsSend(0.4, 0)
                else:
                    motorsSend(-0.4, 0)
                rospy.sleep(0.05)
        elif cur_cmd.startswith("turn"):
            start_pos = cur_pos
            angle = float(cur_cmd.split(" ")[1]) * pi / 180
            while not need_breal and angDist(start_pos, cur_pos) < abs(angle):
                if angle > 0:
                    motorsSend(0, 0.3)
                else:
                    motorsSend(0, -0.3)
                rospy.sleep(0.05)
        elif cur_cmd.startswith("until"):
            while canMove():
                motorsSend(0.4, 0)
                rospy.sleep(0.5)
