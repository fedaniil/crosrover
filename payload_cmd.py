import rospy
import serial
from time import time
from std_msgs.msg import String


INF = 1e18


def getData(data):
    global drill, pulp, hanger_angle, last_time
    arr = data.data.split('|')
    print data.data
    [drill_new, pulp_new, angle_delta] = [int(arr[i]) for i in range(3)]
    duration = float(arr[3])
    drill, pulp = drill_new, pulp_new
    hanger_angle += angle_delta
    last_time = time() + duration
    print str(drill) + " " + str(pulp) + " " + str(hanger_angle) + " " + str(last_time)
    sendParams()


def sendParams():
    global drill, pulp, hanger_angle, serial
    res = str(hanger_angle + 256 * drill + 1024 * pulp)
    print res
    print "serial"
    serial.write(res.encode('utf-8'))


rospy.init_node('payload')
rospy.Subscriber('payload_cmd', String, getData)
serial = serial.Serial('/dev/serial/by-id/usb-Silicon_Labs_CP2102_USB_to_UART_Bridge_Controller_0010-if00-port0', 9600)
drill = 0
pulp = 0
hanger_angle = 0
last_time = 0

while not rospy.is_shutdown():
    if time() >= last_time:
        drill = 0
        pulp = 0
        last_time = INF
        sendParams()
    rospy.sleep(0.05)
