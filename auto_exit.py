import rospy
from std_msgs.msg import String
from sensor_msgs.msg import LaserScan


def getScan(new_scan):
    global scan
    scan = new_scan


rospy.init_node("auto_exit")
rospy.Subscriber("scan", LaserScan, getScan)
move = rospy.Publisher("move", String)
scan = LaserScan()
moved = false

while True:
    if scan.range_max > 1.5:
        move.publish("0.2|0|4.25")
        break
    rospy.sleep(1)
