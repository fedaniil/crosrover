import rospy
import math
from std_msgs.msg import String, Int32


def proceedData(data):
    global linear, angular, pub, drill_rev, pl_d, pl_u, drill_on, pulp_on
    lin_max, ang_max = 0.4, 0.7
    lin_ch, ang_ch = 3, 3
    lin_slide_ch, ang_slide_ch = 10, 10
    new_linear, new_angular, lin_slide, ang_slide, drill_rev1, pl_d1, pl_u1, drill_on1, pulp_on1 = [int(i) for i in data.data.split('|')]
    lin_slide -= 512.0
    if (lin_slide >= 0):
        lin_max *= 1 + (math.floor(lin_slide / round(512 / lin_slide_ch, 2)) / (lin_slide_ch - 1)) * 1
    else:
        lin_max *= 1 + (math.ceil(lin_slide / round(512 / lin_slide_ch, 2)) / (lin_slide_ch - 1)) * 0.5
    ang_slide -= 512.0
    if (ang_slide >= 0):
        ang_max *= 1 + (math.floor(ang_slide / round(512 / ang_slide_ch, 2)) / (ang_slide_ch - 1)) * 1
    else:
        ang_max *= 1 + (math.ceil(ang_slide / round(512 / ang_slide_ch, 2)) / (ang_slide_ch - 1)) * 0.5
    new_linear -= 512.0
    if (new_linear >= 0):
        new_linear = (math.floor(new_linear / round(512 / lin_ch, 2)) / (lin_ch - 1)) * lin_max
    else:
        new_linear = (math.ceil(new_linear / round(512 / lin_ch, 2)) / (lin_ch - 1)) * lin_max
    new_angular -= 512.0
    if (new_angular >= 0):
        new_angular = (math.floor(new_angular / round(512 / ang_ch, 2)) / (ang_ch - 1)) * ang_max
    else:
        new_angular = (math.ceil(new_angular / round(512 / ang_ch, 2)) / (ang_ch - 1)) * ang_max
    new_angular = -new_angular
    if (linear != new_linear or angular != new_angular):
        print str(new_linear) + " " + str(new_angular)
        move.publish(packParams(new_linear, new_angular, 0.5))
        linear, angular = new_linear, new_angular
    if not (drill_rev == drill_rev1 and pl_d == pl_d1 and pl_u == pl_u1 and drill_on == drill_on1 and pulp_on == pulp_on1):
        drill_rev, pl_d, pl_u, drill_on, pulp_on = drill_rev1, pl_d1, pl_u1, drill_on1, pulp_on1
        payload.publish(packPayloadParams())


def packParams(linear, angular, delay):
    return '|'.join(['%.5f' % i for i in [linear, angular, delay]])


def packPayloadParams():
    global drill_rev, pl_d, pl_u, drill_on, pulp_on
    res = 0
    if pl_u:
        res = 20 * 2 + 1
    elif pl_d:
        res = 20 * 2 + 0
    if drill_on and drill_rev:
        res += 2 * 512
    elif drill_on:
        res += 1 * 512
    res += 2048 * pulp_on
    num = res
    up = num % 2
    lift_pos = (num >> 1) % 256
    drill_pos = (num >> 9) % 4
    pump = num >> 11
    print str(up) + " " + str(lift_pos) + " " + str(drill_pos) + " " + str(pump)
    return res


linear = 0.0
angular = 0.0
drill_rev = 0
pl_d = 0
pl_u = 0
drill_on = 0
pulp_on = 0
rospy.init_node('joystick_convert')
move = rospy.Publisher('move', String)
payload = rospy.Publisher('payload_cmd', Int32)
rospy.Subscriber('joystick', String, proceedData)

while not rospy.is_shutdown():
    move.publish(packParams(linear, angular, 0.5))
    payload.publish(packPayloadParams())
    print "auto continue"
    rospy.sleep(0.5)
