#include <ros.h>
#include <std_msgs/Int32.h>
#include <std_msgs/String.h>
#include <Servo.h>

int globalData = 0;

class NewHardware : public ArduinoHardware
{
  public:
  NewHardware():ArduinoHardware(&Serial1, 115200){};
};

ros::NodeHandle_<NewHardware> nh;

void getData(const std_msgs::Int32 &msg) {
  globalData = msg.data;
}

std_msgs::String log_data;
ros::Subscriber<std_msgs::Int32> sub("payload_cmd", &getData);
//ros::Publisher pub("arduino", &log_data);

#define PUMP 0
#define DRILL1 1
#define DRILL2 2
#define LIFT1 44
Servo lift1Servo;
Servo lift2Servo;
int liftPosition = 0;
int drillPosition = 0;

void setup() {
  nh.initNode();
  nh.subscribe(sub);
  pinMode(PUMP, OUTPUT);
  pinMode(DRILL1, OUTPUT);
  pinMode(DRILL2, OUTPUT);
  lift1Servo.attach(LIFT1);
}

void loop() {
  nh.spinOnce();
  int curData = globalData;
  int up = curData % 2;
  if (up) {
    liftPosition = 180;//(curData >> 1) % 256; //расчет состояния подъемника
  } else {
    liftPosition = 0;//(curData >> 1) % 256;
  }
  /*if (liftPosition > 180 || liftPosition < 0) {
    lift1Servo.write(0);
  } else {*/
    lift1Servo.write(liftPosition);
  //}
  drillPosition = (curData >> 9) % 4;
  if (drillPosition == 0) { //сверло отключить
    digitalWrite(DRILL1, HIGH);
    digitalWrite(DRILL2, HIGH);
  }
  else if (drillPosition == 1) { //сверло вниз
    digitalWrite(DRILL1, HIGH);
    digitalWrite(DRILL2, LOW);
  }
  else if (drillPosition == 2) { //сверло вверх
    digitalWrite(DRILL1, LOW);
    digitalWrite(DRILL2, HIGH);
  }
  if ((curData >> 11) == 0) { //включение насоса
    digitalWrite(PUMP, HIGH);
  }
  else if ((curData >> 11) == 1) { //выключение насоса
    digitalWrite(PUMP, LOW);
  }
  String log_cur = String(up) + " " + String(liftPosition) + " " + String(drillPosition) + " " + String(curData >> 11);
  //char bytes_log[1024];
  //log_cur.toCharArray(bytes_log, 1024);
  //log_data.data = bytes_log;
  //pub.publish(&log_data);
}
