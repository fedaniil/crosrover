import rospy
from std_msgs.msg import String
import requests

SERVER = raw_input("Server address: ")
rospy.init_node('joystick_reciever');
pub = rospy.Publisher('joystick', String);

while not rospy.is_shutdown():
    data = requests.get(SERVER);
    print data.text
    pub.publish(data.text);
    rospy.sleep(0.02);
