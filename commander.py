import rospy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from time import time


INF = 1e18


def sendMotors(xl, yl, zl, xa, ya, za):
    global cmd_vel
    msg = Twist()
    msg.linear.x = xl
    msg.linear.y = yl
    msg.linear.z = zl
    msg.angular.x = xa
    msg.angular.y = ya
    msg.angular.z = za
    cmd_vel.publish(msg);


def sendStop():
    sendMotors(0, 0, 0, 0, 0, 0);


def getMove(data):
    global last_move, linear, angular
    [new_linear, new_angular, duration] = [float(i) for i in data.data.split('|')];
    if (new_linear != linear or new_angular != angular):
        sendMotors(new_linear, 0, 0, 0, 0, new_angular)
        linear, angular = new_linear, new_angular
    last_move = time() + duration


rospy.init_node('commander');

last_move = 0
linear = 0.0
angular = 0.0
cmd_vel = rospy.Publisher('cmd_vel', Twist);
rospy.Subscriber('move', String, getMove);

while True:
    cur_time = time();
    if (cur_time >= last_move):
        sendStop()
        last_move = INF;
    rospy.sleep(0.05)
